object App {
	def main(args: Array[String]): Unit = {
		var graphe = Map(
			('a', Map(('b', 1.0), ('f', 5.0))),
			('b', Map(('a', 1.0), ('c', 2.0), ('f',2.0))),
			('c', Map(('b', 2.0), ('d', 2.0), ('e', 3.0), ('f', 15.0))),
			('d', Map(('c', 2.0), ('e', 1.0))),
			('e', Map(('c', 3.0), ('d', 1.0), ('f', 1.0))),
			('f', Map(('a', 5.0), ('b', 2.0), ('c', 15.0), ('e', 1.0)))
		)
		var g = new Graphe(graphe)
		g.dijktra('a','f')
		g createDot "test"
	}
}