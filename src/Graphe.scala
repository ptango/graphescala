import java.io.PrintWriter
import scala.sys.process._
import scala.collection.mutable.{Map => MMap}

class Graphe(_graphe : Map[Char, Map[Char, Double]]) {
	
	var graphe : Map[Char, Map[Char, Double]] = _graphe
	var coursChemain : List[Char] = List()

	def toGraphviz() : String = {
		var str = "digraph G {\n rankdir=BT; \n node [shape = circle]; \n edge[dir=none] \n "
		graphe.foreach({
			case (x, y) => y.foreach{
				m => 
					if(coursChemain.contains(x) && coursChemain.contains(m._1) )
						str += " " + x + "->" + m._1 + "[label=\"" + m._2 + "\" color=\"red\"];" + "\n" 
					else
						str += " " + x + "->" + m._1 + "[label=\"" + m._2 + "\"];" + "\n" 
					
			}
		})
		str + "}"
	}

	def createDot(filename: String) {
		var dotfile = new PrintWriter(filename + ".dot", "UTF-8");
		dotfile.println(this.toGraphviz())
		dotfile.close()
		Process("dot -Tpng " + filename + ".dot -o " + filename + ".png")!
	}

	def dijktra(sommetDep: Char, sommetArr: Char) {
		var x : Char = sommetDep
		var m : List[Char] = List(x)
		var d : MMap[Char, Double] = MMap()
		graphe.foreach(x => d += ((x._1, Double.PositiveInfinity)))
		var p : MMap[Char, Char] = MMap()
		graphe.foreach(x => p += ((x._1, ' ')))

		var minValue : (Char,Double) = (sommetDep,0)

		while(this.graphe.size >= m.size){
			var succ = getSuccesseurs(x)

			def parc(list: List[Char]) : Int = list match {
				case head :: tail => 
					if(graphe(x)(head) + minValue._2 < d(head)){
						d(head) = graphe(x)(head) + minValue._2
						p(head) = x
					}			
					return parc(tail)
				case Nil => return 0
			}
			parc(succ)

			var succWithoutM : List[Char] = List()
			graphe.foreach(x => succWithoutM = succWithoutM :+ x._1)
			succWithoutM = succWithoutM.filterNot(m.toSet)

			minValue = (' ',Double.PositiveInfinity)
			succWithoutM.foreach(xx => if(d(xx) < minValue._2) minValue = (xx, d(xx)))
			x = minValue._1
			m = m :+ x
		}

		def findPath(sommet : Char) : Unit = {
			if(coursChemain.isEmpty){
				coursChemain = List(sommet)
			}else{
				coursChemain = coursChemain :+ sommet
			}
			
			if(sommet != sommetDep){
				return findPath(p(sommet))
			}
		}

		findPath(sommetArr)
		coursChemain = coursChemain.reverse

		println(coursChemain)

	}

	def getSuccesseurs(sommet : Char) = {
		var succ : List[Char] = List()
		val succSommet : Map[Char, Double] = graphe(sommet)
		succSommet.foreach(x => succ = succ :+ x._1)
		succ
	}
}